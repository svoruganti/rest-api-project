
package com.carnews.rest.api.tests.persistence;

import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class ArticleRepositoryTests extends AbstractIntegrationTest {

    @Autowired
    private ContentReadRepository<Article> articleRepository;
    @Test
    public void shouldGetArticleById() {
        Article article = articleRepository.get(1);

        assertNotNull(article);
        assertThat(article, allOf(hasProperty("id", is(1)),
                hasProperty("title", is("article title 1")),
                hasProperty("synopsis", is("article synopsis")),
                hasProperty("metaDescription", is("meta description")),
                hasProperty("description", is("article description")),
                hasProperty("tags", is("article tags"))));
    }

    @Test
    public void shouldOnlyGetArticle() {
        Article article = articleRepository.get(2);
        assertThat(article, is(nullValue()));
    }
}