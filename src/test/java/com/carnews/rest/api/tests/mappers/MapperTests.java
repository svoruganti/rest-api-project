package com.carnews.rest.api.tests.mappers;

import com.carnews.rest.api.configuration.MappingConfiguration;
import com.carnews.rest.api.models.*;
import com.carnews.rest.api.response.*;
import com.carnews.rest.api.tests.models.UnitTest;
import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

@Category(UnitTest.class)
public class MapperTests {

    DozerBeanMapper mapper;
    @Before
    public void setUp() throws Exception {
        mapper = new DozerBeanMapper();
    }

    @Test
    public void mapCarToCarResponseTest(){
        Car car = new Car();
        car.setId(1);
        car.setModel("A6");

        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("Audi");
        car.setManufacturer(manufacturer);

        mapper.addMapping(MappingConfiguration.getCarMapper());

        CarResponse response = mapper.map(car, CarResponse.class);
        assertEquals(response.getMake(), "Audi");
        assertEquals(response.getModel(), "A6");
    }

    @Test
    public void mapDerivativeExtraToDerivativeExtraResponseTest() {
        DerivativeExtra derivativeExtra = new DerivativeExtra();
        DerivativeExtraType derivativeExtraType = new DerivativeExtraType();
        derivativeExtraType.setName("derivativeExtraTypeName");
        derivativeExtra.setType(derivativeExtraType);
        derivativeExtra.setValue("B");

        mapper.addMapping(MappingConfiguration.getDerivativeExtraMapper());

        DerivativeExtraResponse response = mapper.map(derivativeExtra, DerivativeExtraResponse.class);
        assertThat(response.getName(), is("derivativeExtraTypeName"));
        assertThat(response.getValue(), is("B"));
    }

    @Test
    public void mapVideoToVideoResponse() {
        Video video = new Video();
        VideoRating videoRating = new VideoRating();
        videoRating.setName("PG");
        video.setVideoRating(videoRating);

        mapper.addMapping(MappingConfiguration.getVideoMapper());

        VideoResponse response = mapper.map(video, VideoResponse.class);

        assertThat(response.getRating(), is("PG"));
    }

    @Test
    public void mapVerdictToVerdictResponse() {
        Derivative derivative = new Derivative();
        derivative.setName("derivativename");

        Car car = new Car();
        car.setModel("A6");

        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("Audi");

        car.setManufacturer(manufacturer);
        derivative.setCar(car);

        Verdict verdict = new Verdict();
        verdict.setDerivative(derivative);

        mapper.addMapping(MappingConfiguration.getVerdictMapper());

        VerdictResponse response = mapper.map(verdict, VerdictResponse.class);

        assertThat(response.getDerivativeName(), is("derivativename"));
        assertThat(response.getMake(), is("Audi"));
        assertThat(response.getModel(), is("A6"));
    }

    @Test
    public void mapGalleryToGalleryResponse() {
        Gallery gallery = new Gallery();
        gallery.setId(1);
        gallery.setTitle("gallery title");
        gallery.setMetaDescription("gallery meta description");
        gallery.setDescription("gallery description");
        gallery.setTags("gallery tags");
        gallery.setSynopsis("gallery synopsis");

        Image imageOne = new Image();
        imageOne.setId(2);
        imageOne.setAlternateText("image alternate text");
        imageOne.setUrl("image url");
        imageOne.setWidth(75);
        imageOne.setHeight(100);
        imageOne.setAspectRatio(AspectRatio.FourByThree);

        Image imageTwo = new Image();
        imageTwo.setId(3);
        imageTwo.setAlternateText("image alternate text 2");
        imageTwo.setUrl("image url 2");
        imageTwo.setWidth(90);
        imageTwo.setHeight(160);
        imageTwo.setAspectRatio(AspectRatio.SixteenByNine);

        GalleryImage galleryImageOne = new GalleryImage();
        //galleryImageOne.setGallery(gallery);
        galleryImageOne.setImage(imageOne);
        galleryImageOne.setSortOrder(1);

        GalleryImage galleryImageTwo = new GalleryImage();
        //galleryImageTwo.setGallery(gallery);
        galleryImageTwo.setImage(imageTwo);
        galleryImageTwo.setSortOrder(2);

        gallery.setImages(Arrays.asList(galleryImageOne, galleryImageTwo));

        GalleryResponse response = mapper.map(gallery, GalleryResponse.class);
        assertThat(response.getImages(), is(notNullValue()));
        assertThat(response.getImages().size(), is(2));
        String[] titles = new String[2];

        for (int i=0;i<response.getImages().size(); i++)
            titles[i] = response.getImages().get(i).getImage().getAlternateText();
        assertThat(titles, arrayContainingInAnyOrder("image alternate text", "image alternate text 2"));
    }
}
