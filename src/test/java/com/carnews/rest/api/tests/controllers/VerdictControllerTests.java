package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.VerdictController;
import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Derivative;
import com.carnews.rest.api.models.Manufacturer;
import com.carnews.rest.api.models.Verdict;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class VerdictControllerTests extends AbstractControllerTest {
    @Mock
    private ReadRepository<Verdict> repository;

    @Override
    protected Object getController() {
        return new VerdictController(repository, getMapper());
    }

    @Test
    public void shouldReturnVerdict() throws Exception {
        Verdict verdict = new Verdict();
        verdict.setId(1);
        verdict.setComfortRating(9);
        verdict.setComfortText("comfort text");
        verdict.setCoolRating(9);
        verdict.setCoolText("cool text");
        verdict.setHandlingRating(8);
        verdict.setHandlingText("handling text");
        verdict.setPerformanceRating(9);
        verdict.setPerformanceText("performance text");
        verdict.setPracticalityRating(7);
        verdict.setPracticalityText("practicality text");
        verdict.setQualityRating(8.5);
        verdict.setQualityText("quality text");
        verdict.setRunningCostRating(7.2);
        verdict.setRunningCostText("running cost text");
        verdict.setVerdictRating(8);
        verdict.setVerdictText("verdict text");

        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("Audi");
        Car car = new Car();
        car.setManufacturer(manufacturer);
        car.setModel("A6");
        Derivative derivative = new Derivative();
        derivative.setName("A6 2.0");

        verdict.setDerivative(derivative);
        derivative.setCar(car);

        when(repository.get(1)).thenReturn(verdict);
        mockMvc.perform(get("/verdicts/{id}", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.make", is("Audi")))
                .andExpect(jsonPath("$.model", is("A6")))
                .andExpect(jsonPath("$.derivativeName", is("A6 2.0")))
                .andExpect(jsonPath("$.comfortRating", is(9d)))
                .andExpect(jsonPath("$.comfortText", is("comfort text")))
                .andExpect(jsonPath("$.coolRating", is(9d)))
                .andExpect(jsonPath("$.coolText", is("cool text")))
                .andExpect(jsonPath("$.handlingRating", is(8d)))
                .andExpect(jsonPath("$.handlingText", is("handling text")))
                .andExpect(jsonPath("$.performanceRating", is(9d)))
                .andExpect(jsonPath("$.performanceText", is("performance text")))
                .andExpect(jsonPath("$.practicalityRating", is(7d)))
                .andExpect(jsonPath("$.practicalityText", is("practicality text")))
                .andExpect(jsonPath("$.qualityRating", is(8.5d)))
                .andExpect(jsonPath("$.qualityText", is("quality text")))
                .andExpect(jsonPath("$.runningCostRating", is(7.2d)))
                .andExpect(jsonPath("$.runningCostText", is("running cost text")))
                .andExpect(jsonPath("$.verdictRating", is(8d)))
                .andExpect(jsonPath("$.verdictText", is("verdict text")))
                .andExpect(jsonPath("$.links[0].href", is("/verdicts/1")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }
}
