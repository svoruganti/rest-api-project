package com.carnews.rest.api.tests.persistence;


import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Verdict;
import com.carnews.rest.api.persistence.interfaces.CarRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CarRepositoryTests extends AbstractIntegrationTest {
    @Autowired
    private CarRepository carRepository;

    @Test
    public void shouldFindVerdictsByCarId() {
        Collection<Verdict> verdicts = carRepository.findVerdictsByCar(1);

        assertThat(verdicts.getCount(), is(1));

        Verdict verdict = verdicts.getItems().get(0);
        assertThat(verdict.getVerdictRating(), is(9.0));
        assertThat(verdict.getVerdictText(), is("test verdict text"));
        assertThat(verdict.getComfortRating(), is(9.0));
        assertThat(verdict.getComfortText(), is("test comfort text"));
        assertThat(verdict.getCoolRating(), is(9.0));
        assertThat(verdict.getCoolText(), is("test cool text"));
    }
}
