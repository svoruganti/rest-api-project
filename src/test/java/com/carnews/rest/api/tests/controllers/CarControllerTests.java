package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.CarController;
import com.carnews.rest.api.exceptions.CollectionEmptyException;
import com.carnews.rest.api.exceptions.ItemNotFoundException;
import com.carnews.rest.api.models.*;
import com.carnews.rest.api.persistence.interfaces.CarRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class CarControllerTests extends AbstractControllerTest {

    @Mock
    private CarRepository repository;

    protected Object getController() {
        return new CarController(repository, getMapper());
    }

    @Test
    public void shouldReturnNotFoundResponseWhenCarNotFound() throws Exception {
        setExpectedExceptionNestedClass(ItemNotFoundException.class);
        when(repository.get(anyInt())).thenReturn(null);
        mockMvc.perform(get("/cars/{id}", 1)).andExpect(status().isNotFound());
        verify(repository, times(1)).get(anyInt());
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldReturnNoContentResponseWhenCollectionIsEmpty() throws Exception {
        setExpectedExceptionNestedClass(CollectionEmptyException.class);
        when(repository.findVerdictsByCar(1)).thenReturn(new Collection<Verdict>(0, null));
        mockMvc.perform(get("/cars/{id}/verdicts", 1)).andExpect(status().isNoContent());
        verify(repository, times(1)).findVerdictsByCar(1);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldReturnCar() throws Exception {
        Car car = new Car();
        car.setId(1);
        car.setModel("A6");

        when(repository.get(1)).thenReturn(car);
        mockMvc.perform(get("/cars/{id}", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.model", is("A6")))
                .andExpect(jsonPath("$.links[?(@.rel=='self')][0].href", is("/cars/1")))
                .andExpect(jsonPath("$.links[?(@.rel=='verdicts')][0].href", is("/cars/1/verdicts")))
                .andExpect(jsonPath("$.links[?(@.rel=='derivatives')][0].href", is("/cars/1/derivatives")))
                .andExpect(jsonPath("$.links[?(@.rel=='videos')][0].href", is("/cars/1/videos")))
                .andExpect(jsonPath("$.links[?(@.rel=='rivals')][0].href", is("/cars/1/rivals")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldReturnCarVerdicts() throws Exception {
        Verdict verdictOne = new Verdict();
        verdictOne.setId(50);
        verdictOne.setComfortRating(9.0);
        verdictOne.setComfortText("Very comfort");

        Verdict verdictTwo = new Verdict();
        verdictTwo.setId(75);
        verdictTwo.setComfortRating(8.5);
        verdictTwo.setComfortText("Some how comfort");

        when(repository.findVerdictsByCar(1)).thenReturn(new Collection<Verdict>(2, Arrays.asList(verdictOne, verdictTwo)));
        mockMvc.perform(get("/cars/{id}/verdicts", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.count", is(2)))
                .andExpect(jsonPath("$.items[*].comfortText", containsInAnyOrder("Very comfort", "Some how comfort")))
                .andExpect(jsonPath("$.items[*].comfortRating", containsInAnyOrder(9.0, 8.5)))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='self')].href", containsInAnyOrder("/verdicts/50", "/verdicts/75")));
        verify(repository, times(1)).findVerdictsByCar(1);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldReturnCarDerivatives() throws Exception {
        Car car = new Car();
        car.setId(1);
        Derivative derivative = new Derivative();
        derivative.setId(100);
        derivative.setName("Derivative name");
        derivative.setFuelType(FuelType.diesel);
        derivative.setMinPrice(10000);
        derivative.setMaxPrice(20000);
        derivative.setTrim("nice trim");
        DerivativeExtraType derivativeExtraType = new DerivativeExtraType();
        derivativeExtraType.setName("derivative extra type");
        DerivativeExtra derivativeExtra = new DerivativeExtra();
        derivativeExtra.setType(derivativeExtraType);
        derivativeExtra.setValue("value");
        derivative.setExtras(Arrays.asList(derivativeExtra));
        car.setDerivatives(Arrays.asList(derivative));

        when(repository.get(1)).thenReturn(car);
        mockMvc.perform(get("/cars/{id}/derivatives", 1).accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.count", is(1)))
                .andExpect(jsonPath("$.items[0].name", is("Derivative name")))
                .andExpect(jsonPath("$.items[0].fuelType", is("diesel")))
                .andExpect(jsonPath("$.items[0].minPrice", is(10000d)))
                .andExpect(jsonPath("$.items[0].maxPrice", is(20000d)))
                .andExpect(jsonPath("$.items[0].trim", is("nice trim")))
                .andExpect(jsonPath("$.items[0].extras[0].name", is("derivative extra type")))
                .andExpect(jsonPath("$.items[0].extras[0].value", is("value")))
                .andExpect(jsonPath("$.items[0].links[0].href", is("/derivatives/100")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldReturnCarVideos() throws Exception {
        VideoRating videoRating = new VideoRating();
        videoRating.setName("video rating");

        Video video = new Video();
        video.setId(10);
        video.setTitle("video title");
        video.setTags("video tags");
        video.setSynopsis("video synopsis");
        video.setMetaDescription("video meta description");
        video.setVideoRating(videoRating);

        Car car = new Car();
        car.setVideos(Arrays.asList(video));

        when(repository.get(1)).thenReturn(car);
        mockMvc.perform(get("/cars/{id}/videos", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.count", is(1)))
                .andExpect(jsonPath("$.items[0].title", is("video title")))
                .andExpect(jsonPath("$.items[0].tags", is("video tags")))
                .andExpect(jsonPath("$.items[0].synopsis", is("video synopsis")))
                .andExpect(jsonPath("$.items[0].metaDescription", is("video meta description")))
                .andExpect(jsonPath("$.items[0].rating", is("video rating")))
                .andExpect(jsonPath("$.items[0].links[0].href", is("/videos/10")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void shouldReturnCarRivalCars() throws Exception {
        Car car = new Car();
        Car rivalCar = new Car();
        rivalCar.setId(2);
        rivalCar.setModel("X5");
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("BMW");
        rivalCar.setManufacturer(manufacturer);
        car.setRivals(Arrays.asList(rivalCar));

        when(repository.get(1)).thenReturn(car);
        mockMvc.perform(get("/cars/{id}/rivals", 1).accept(APPLICATION_JSON_UTF8))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.count", is(1)))
                .andExpect(jsonPath("$.items[0].make", is("BMW")))
                .andExpect(jsonPath("$.items[0].model", is("X5")))
                .andExpect(jsonPath("$.items[0].links[?(@.rel=='self')][0].href", is("/cars/2")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }
}
