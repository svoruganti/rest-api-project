package com.carnews.rest.api.tests.models;

import com.carnews.rest.api.models.Car;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertEquals;

@Category(UnitTest.class)
public class CarTests {

    @Test
    public void testGetSetModel() throws Exception {
        Car car = new Car();
        car.setModel("A6");
        assertEquals("A6", car.getModel());
    }
}
