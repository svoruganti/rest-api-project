package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.configuration.MappingConfiguration;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.nio.charset.Charset;

import static org.hamcrest.Matchers.is;

public abstract class AbstractControllerTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    protected MockMvc mockMvc;
    protected final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(getController()).build();
    }

    protected abstract Object getController();

    protected Mapper getMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();
        for(BeanMappingBuilder builder : MappingConfiguration.getBeanMappingBuilders())
            mapper.addMapping(builder);
        return mapper;
    }

    protected void setExpectedExceptionNestedClass(Class<? extends Exception> clazz) {
        expectedException.expectCause(is(IsInstanceOf.<Throwable>instanceOf(clazz)));
    }
}
