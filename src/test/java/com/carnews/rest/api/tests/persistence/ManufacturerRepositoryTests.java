package com.carnews.rest.api.tests.persistence;

import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Manufacturer;
import com.carnews.rest.api.persistence.interfaces.ManufacturerRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class ManufacturerRepositoryTests extends AbstractIntegrationTest {
    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @Test
    public void shouldGetAllManufacturers() {
        Collection<Manufacturer> manufacturers = manufacturerRepository.getAll();

        assertThat(manufacturers.getCount(), is(equalTo(2)));

        List<String> names = new ArrayList<String>();
        for(Manufacturer m : manufacturers.getItems())
            names.add(m.getName());
        assertThat(names, containsInAnyOrder("BMW", "Audi"));
    }

    @Test
    public void shouldGetAllCarsByManufacturer() {
        Collection<Car> cars = manufacturerRepository.getAllCarsByManufacturer(1);

        assertThat(cars.getCount(), is(equalTo(1)));

        Car car = cars.getItems().get(0);
        assertThat(car.getModel(), is("A6"));
    }
}
