package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.ManufacturerController;
import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Manufacturer;
import com.carnews.rest.api.persistence.interfaces.ManufacturerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ManufacturerControllerTests extends AbstractControllerTest {

    @Mock
    private ManufacturerRepository repository;

    @Override
    protected Object getController() {
        return new ManufacturerController(repository, getMapper());
    }
/*
    @Test
    public void shouldReturnAllManufacturers() throws Exception {
        Manufacturer manufacturerOne = new Manufacturer();
        manufacturerOne.setName("Audi");
        manufacturerOne.setId(1);

        Manufacturer manufacturerTwo = new Manufacturer();
        manufacturerTwo.setName("BMW");
        manufacturerTwo.setId(2);

        when(repository.getAll()).thenReturn(new Collection<Manufacturer>(2, Arrays.asList(manufacturerOne, manufacturerTwo)));
        mockMvc.perform(get("/manufacturers").accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.count", is(2)))
                .andExpect(jsonPath("$.items[*].name", containsInAnyOrder("Audi", "BMW")))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='self')].href", containsInAnyOrder("/manufacturers/1", "/manufacturers/2")))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='cars')].href", containsInAnyOrder("/manufacturers/1/cars", "/manufacturers/2/cars")));
        verify(repository, times(1)).getAll();
        verifyNoMoreInteractions(repository);
    }
*/
    @Test
    public void shouldReturnCarsByManufacturer() throws Exception {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("Audi");
        manufacturer.setId(1);

        Car carOne = new Car();
        carOne.setId(2);
        carOne.setModel("A4");
        carOne.setManufacturer(manufacturer);

        Car carTwo = new Car();
        carTwo.setId(3);
        carTwo.setModel("A6");
        carTwo.setManufacturer(manufacturer);

        when(repository.getAllCarsByManufacturer(1)).thenReturn(new Collection<Car>(2, Arrays.asList(carOne, carTwo)));
        mockMvc.perform(get("/manufacturers/{id}/cars", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.count", is(2)))
                .andExpect(jsonPath("$.items[*].model", containsInAnyOrder("A4", "A6")))
                .andExpect(jsonPath("$.items[*].make", containsInAnyOrder("Audi", "Audi")))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='self')].href", containsInAnyOrder("/cars/2", "/cars/3")))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='verdicts')].href", containsInAnyOrder("/cars/2/verdicts", "/cars/3/verdicts")))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='videos')].href", containsInAnyOrder("/cars/2/videos", "/cars/3/videos")))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='derivatives')].href", containsInAnyOrder("/cars/2/derivatives", "/cars/3/derivatives")))
                .andExpect(jsonPath("$.items[*].links[?(@.rel=='rivals')].href", containsInAnyOrder("/cars/2/rivals", "/cars/3/rivals")));
        verify(repository, times(1)).getAllCarsByManufacturer(1);
        verifyNoMoreInteractions(repository);
    }
}
