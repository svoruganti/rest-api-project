package com.carnews.rest.api.tests.persistence;

import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractContentReadRepositoryTests extends AbstractIntegrationTest {
    @Autowired
    private ContentReadRepository<Article> repository;

    @Test
    public void shouldGetArticleById() {
        /*
        Collection<Article> articles = repository.findLatest(25, 0);

        assertThat(articles.getCount(), is(1));
        assertThat(articles.getItems().get(0), allOf(hasProperty("id", is(1)),
                hasProperty("title", is("article title 1")),
                hasProperty("synopsis", is("article synopsis")),
                hasProperty("metaDescription", is("meta description")),
                hasProperty("description", is("article description")),
                hasProperty("tags", is("article tags"))));
                */
    }
}
