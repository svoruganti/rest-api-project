package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.DefaultController;
import org.junit.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DefaultControllerTests extends AbstractControllerTest{
    @Override
    protected Object getController() {
        return new DefaultController();
    }

    @Test
    public void shouldReturnDefaultResponse() throws Exception {
        mockMvc.perform(get("/").accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.links", hasSize(5)))
                .andExpect(jsonPath("$.links[?(@rel=='latest')][0].href", is("/latest")))
                .andExpect(jsonPath("$.links[?(@rel=='articleLatest')][0].href", is("/articles/latest")))
                .andExpect(jsonPath("$.links[?(@rel=='videoLatest')][0].href", is("/videos/latest")))
                .andExpect(jsonPath("$.links[?(@rel=='galleryLatest')][0].href", is("/galleries/latest")))
                .andExpect(jsonPath("$.links[?(@rel=='manufacturers')][0].href", is("/manufacturers")));
    }
}
