package com.carnews.rest.api.tests.helper;

import com.carnews.rest.api.helper.StringHelper;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class StringHelperTests {
    @Test
    public void shouldRemoveSpecialCharacters(){
        String s = "This is,a special! text";
        String result = StringHelper.sanitiseString(s);
        assertThat(result, is(equalTo("This is a special text")));
    }

    @Test
    public void shouldFormatEmptyString() {
        String s = "";
        String result = StringHelper.sanitiseString(s);
        assertThat(result, is(equalTo("")));
    }

    @Test
    public void shouldFormatStringWithOnlyWhiteSpaces() {
        String s = "   ";
        String result = StringHelper.sanitiseString(s);
        assertThat(result, is(equalTo("")));
    }
}
