package com.carnews.rest.api.tests.persistence;

import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.persistence.interfaces.ContentRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class SearchRepositoryTests extends AbstractIntegrationTest {
    @Autowired
    private ContentRepository contentRepository;

    @Test
    public void shouldFindContentByTitle() {
        Collection<Content> result = contentRepository.search("title", 0, 10);
        assertThat(result.getCount(), is(3));
    }
}
