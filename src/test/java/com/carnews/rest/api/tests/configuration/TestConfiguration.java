package com.carnews.rest.api.tests.configuration;

import com.carnews.rest.api.persistence.interfaces.CarRepository;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class TestConfiguration {

    @Bean
    public CarRepository carRepository() {
        return Mockito.mock(CarRepository.class);
    }

    private static final String MESSAGE_SOURCE_BASE_NAME = "test/messages";

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

        messageSource.setBasename(MESSAGE_SOURCE_BASE_NAME);
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }
}
