package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.GalleryController;
import com.carnews.rest.api.models.AspectRatio;
import com.carnews.rest.api.models.Gallery;
import com.carnews.rest.api.models.GalleryImage;
import com.carnews.rest.api.models.Image;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class GalleryControllerTests extends AbstractControllerTest {

    @Mock
    private ContentReadRepository<Gallery> repository;

    @Override
    protected Object getController() {
        return new GalleryController(repository, getMapper());
    }

    @Test
    public void shouldReturnGallery() throws Exception {
        Gallery gallery = new Gallery();
        gallery.setId(1);
        gallery.setTitle("gallery title");
        gallery.setMetaDescription("gallery meta description");
        gallery.setDescription("gallery description");
        gallery.setTags("gallery tags");
        gallery.setSynopsis("gallery synopsis");
        Image image = new Image();
        image.setId(2);
        image.setAlternateText("image alternate text");
        image.setUrl("image url");
        image.setWidth(75);
        image.setHeight(100);
        image.setAspectRatio(AspectRatio.FourByThree);

        GalleryImage galleryImage = new GalleryImage();
        //galleryImage.setGallery(gallery);
        galleryImage.setImage(image);
        galleryImage.setSortOrder(1);

        gallery.setImages(Arrays.asList(galleryImage));

        when(repository.get(1)).thenReturn(gallery);
        mockMvc.perform(get("/galleries/1").accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.links[0].href", is("/galleries/1")))
                .andExpect(jsonPath("$.title", is("gallery title")))
                .andExpect(jsonPath("$.metaDescription", is("gallery meta description")))
                .andExpect(jsonPath("$.description", is("gallery description")))
                .andExpect(jsonPath("$.tags", is("gallery tags")))
                .andExpect(jsonPath("$.synopsis", is("gallery synopsis")))
                .andExpect(jsonPath("$.images[0].sortOrder", is(1)))
                //.andExpect(jsonPath("$.images[0].image.href", is("/images/2")))
                .andExpect(jsonPath("$.images[0].image.alternateText", is("image alternate text")))
                .andExpect(jsonPath("$.images[0].image.width", is(75)))
                .andExpect(jsonPath("$.images[0].image.height", is(100)))
                .andExpect(jsonPath("$.images[0].image.aspectRatio", is("FourByThree")))
                .andExpect(jsonPath("$.images[0].image.url", is("image url")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }
}
