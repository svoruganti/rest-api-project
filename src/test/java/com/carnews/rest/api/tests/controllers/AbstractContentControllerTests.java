package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.ArticleController;
import com.carnews.rest.api.exceptions.CheckLengthException;
import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Calendar;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class AbstractContentControllerTests extends AbstractControllerTest{

    @Mock
    private ContentReadRepository<Article> repository;

    @Override
    protected Object getController() {
        return new ArticleController(repository, getMapper());
    }

    @Test
    public void shouldReturnBadRequestRequestWhenNumberOfRequestedReturnResultsIsMoreThan100() throws Exception {
        setExpectedExceptionNestedClass(CheckLengthException.class);
        mockMvc.perform(get("/articles/latest").param("skip", "1").param("size", "101"))
                .andExpect(status().isBadRequest());
        verify(repository, never()).findLatest(101, 1);
    }

    @Test
    public void shouldGetLatestArticles() throws Exception {
        Article article = new Article();
        article.setId(1);
        article.setTitle("article title");
        article.setDescription("article description");
        article.setMetaDescription("article meta description");
        article.setSynopsis("article synopsis");
        article.setTags("article tags");
        article.setPublishedDate(Calendar.getInstance().getTime());

        when(repository.findLatest(100, 0)).thenReturn(new Collection<Article>(1, Arrays.asList(article)));
        mockMvc.perform(get("/articles/latest").accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.count", is(1)))
                .andExpect(jsonPath("$.items[0].title", is("article title")))
                .andExpect(jsonPath("$.items[0].description", is("article description")))
                .andExpect(jsonPath("$.items[0].metaDescription", is("article meta description")))
                .andExpect(jsonPath("$.items[0].synopsis", is("article synopsis")))
                .andExpect(jsonPath("$.items[0].tags", is("article tags")))
                .andExpect(jsonPath("$.items[0].links[0].href", is("/articles/1")));
        verify(repository, times(1)).findLatest(100, 0);
        verifyNoMoreInteractions(repository);
    }
}
