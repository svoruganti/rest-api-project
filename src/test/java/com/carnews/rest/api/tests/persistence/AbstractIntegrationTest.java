package com.carnews.rest.api.tests.persistence;

import com.carnews.rest.api.configuration.HibernateConfiguration;
import com.googlecode.flyway.core.Flyway;
import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.IDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlProducer;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.InputSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class, })
@Category(IntegrationTest.class)
public abstract class AbstractIntegrationTest{
    @Autowired
    private DataSource dataSource;

    @Before
    public void setUp() throws Exception{
        try {
            Flyway flyway = new Flyway();
            flyway.setDataSource(dataSource);
            flyway.migrate();

            IDatabaseTester databaseTester = getDataSourceDatabaseTester();

            databaseTester.setTearDownOperation(DatabaseOperation.TRUNCATE_TABLE);
            IDataSet dataSet = getDataSet();
            databaseTester.setDataSet(dataSet);
            databaseTester.onSetup();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private DataSourceDatabaseTester getDataSourceDatabaseTester() {
        return new DataSourceDatabaseTester(dataSource){
          @Override public IDatabaseConnection getConnection() throws Exception {
              IDatabaseConnection connection = super.getConnection();
              connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new PostgresqlDataTypeFactory());
              return connection;
          }
        };
    }

    private IDataSet getDataSet() throws DataSetException, FileNotFoundException {
        return new FlatXmlDataSet(new FlatXmlProducer(new InputSource(new FileInputStream(getXmlFileName()))));
    }

    private String getXmlFileName(){
        return "src/test/resources/com/carnews/rest/api/tests/persistence/testData.xml";
    }
}
