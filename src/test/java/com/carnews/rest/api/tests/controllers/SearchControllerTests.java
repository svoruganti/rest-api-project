package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.SearchController;
import com.carnews.rest.api.exceptions.CheckLengthException;
import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.models.Gallery;
import com.carnews.rest.api.persistence.interfaces.ContentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class SearchControllerTests extends AbstractControllerTest {

    @Mock
    private ContentRepository repository;

    @Override
    protected Object getController() {
        return new SearchController(repository, getMapper());
    }

    @Test
    public void shouldReturnBadRequestRequestWhenNumberOfRequestedReturnResultsIsMoreThan100() throws Exception {
        setExpectedExceptionNestedClass(CheckLengthException.class);
        mockMvc.perform(get("/search").param("q", "title").param("skip", "1").param("size", "101"))
                .andExpect(status().isBadRequest());
        verify(repository, never()).search("title", 101, 1);
    }

    @Test
    public void shouldReturnBadRequestResponseWhenLengthOfSearchTermIsLessThan4Characters() throws Exception {
        setExpectedExceptionNestedClass(CheckLengthException.class);
        mockMvc.perform(get("/search")).andExpect(status().isBadRequest());
        verify(repository, never()).search(anyString(), anyInt(), anyInt());
    }

    @Test
    public void shouldReturnBadRequestResponseWhenLengthOfSearchTermIsMoreThan16Characters() throws Exception {
        setExpectedExceptionNestedClass(CheckLengthException.class);
        mockMvc.perform(get("/search").param("q", "this is a test string to check length"))
                .andExpect(status().isBadRequest());
        verify(repository, never()).search(anyString(), anyInt(), anyInt());
    }

    @Test
    public void shouldReturnSearchResults() throws Exception {
        Content article = new Article();
        article.setTitle("article title");
        article.setTags("article tags");
        article.setSynopsis("article synopsis");
        article.setMetaDescription("article meta description");
        article.setId(1);

        Content gallery = new Gallery();
        gallery.setTitle("gallery title");
        gallery.setTags("gallery tags");
        gallery.setSynopsis("gallery synopsis");
        gallery.setMetaDescription("gallery meta description");
        gallery.setId(2);

        when(repository.search(anyString(), anyInt(), anyInt())).thenReturn(new Collection<Content>(2, Arrays.asList(article, gallery)));
        mockMvc.perform(get("/search").param("q", "title").accept(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.count", is(2)))
                .andExpect(jsonPath("$.items[*].title", containsInAnyOrder("article title", "gallery title")))
                .andExpect(jsonPath("$.items[*].tags", containsInAnyOrder("article tags", "gallery tags")))
                .andExpect(jsonPath("$.items[*].synopsis", containsInAnyOrder("article synopsis", "gallery synopsis")));
                //.andExpect(jsonPath("$.items[*].links[?(@.rel=='self')].href", containsInAnyOrder("/articles/1", "/galleries/2")));
        verify(repository, times(1)).search(anyString(), anyInt(), anyInt());
        verifyZeroInteractions(repository);
    }
}
