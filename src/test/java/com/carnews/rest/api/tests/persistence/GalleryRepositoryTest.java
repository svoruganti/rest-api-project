package com.carnews.rest.api.tests.persistence;

import com.carnews.rest.api.models.AspectRatio;
import com.carnews.rest.api.models.Gallery;
import com.carnews.rest.api.models.Image;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.arrayContainingInAnyOrder;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
@Category(IntegrationTest.class)
public class GalleryRepositoryTest extends AbstractIntegrationTest {
    @Autowired
    private ReadRepository<Gallery> galleryRepository;

    @Test
    public void shouldGetAllImagesOfAGallery() {
        Gallery gallery = galleryRepository.get(2);
        assertNotNull(gallery);
        assertThat(gallery.getImages().size(), is(2));
        String[] alternativeTexts = new String[2];
        AspectRatio[] aspectRatio = new AspectRatio[2];
        int[] sortOrders = new int[2];

        for(int i=0; i < gallery.getImages().size(); i++) {
            Image image = gallery.getImages().get(i).getImage();
            alternativeTexts[i] = image.getAlternateText();
            aspectRatio[i] = image.getAspectRatio();
            sortOrders[i] = gallery.getImages().get(i).getSortOrder();
        }

        assertThat(alternativeTexts, arrayContainingInAnyOrder("Image 1 alternative text", "Image 2 alternative text"));
        assertThat(aspectRatio, arrayContainingInAnyOrder(AspectRatio.FourByThree, AspectRatio.SixteenByNine));
    }
}
