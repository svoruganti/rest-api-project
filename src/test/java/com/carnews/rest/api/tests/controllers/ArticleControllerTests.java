package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.ArticleController;
import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Calendar;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ArticleControllerTests extends AbstractControllerTest {

    @Mock
    private ContentReadRepository<Article> repository;

    @Override
    protected Object getController() {
        return new ArticleController(repository, getMapper());
    }

    @Test
    public void shouldGetArticle() throws Exception {
        Article article = new Article();
        article.setId(1);
        article.setTitle("article title");
        article.setDescription("article description");
        article.setMetaDescription("article meta description");
        article.setSynopsis("article synopsis");
        article.setTags("article tags");
        article.setPublishedDate(Calendar.getInstance().getTime());

        when(repository.get(1)).thenReturn(article);

        mockMvc.perform(get("/articles/{id}", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.title", is("article title")))
                .andExpect(jsonPath("$.description", is("article description")))
                .andExpect(jsonPath("$.metaDescription", is("article meta description")))
                .andExpect(jsonPath("$.synopsis", is("article synopsis")))
                .andExpect(jsonPath("$.tags", is("article tags")))
                .andExpect(jsonPath("$.links[0].href", is("/articles/1")));
        //TODO: Need to find out a way to convert java date object to json object for testing published date.
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }
}
