package com.carnews.rest.api.tests.persistence;

import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.hibernate.Cache;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManagerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Category(IntegrationTest.class)
public class HibernateSecondLevelCachingTests extends AbstractIntegrationTest{
    @Autowired
    private EntityManagerFactory entityManager;
    @Autowired
    private ReadRepository<Car> carReadRepository;
    @Autowired
    private ReadRepository<Article> articleContentReadRepository;

    @Test
    public void shouldRetrieveNonContentDataFromCache() {
        Car car = carReadRepository.get(1);
        Statistics statistics = getStatistics();
        assertThat(statistics.getSecondLevelCacheHitCount(), equalTo(0L));

        Car newCar = carReadRepository.get(1);
        assertThat(statistics.getSecondLevelCacheHitCount(), equalTo(1L));
    }

    @Test
    public void shouldRetrieveContentDataFromCache() {
        Article article = articleContentReadRepository.get(1);
        Statistics statistics = getStatistics();
        assertThat(statistics.getSecondLevelCacheHitCount(), equalTo(0L));

        Article newArticle = articleContentReadRepository.get(1);
        assertThat(statistics.getSecondLevelCacheHitCount(), equalTo(1L));
    }

    private Statistics getStatistics() {
        Session session = (Session)entityManager.createEntityManager().getDelegate();
        Statistics statistics = session.getSessionFactory().getStatistics();
        statistics.clear();
        return statistics;
    }
}
