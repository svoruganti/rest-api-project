package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.VideoController;
import com.carnews.rest.api.models.Video;
import com.carnews.rest.api.models.VideoRating;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class VideoControllerTests extends AbstractControllerTest {

    @Mock
    private ContentReadRepository<Video> repository;

    @Override
    protected Object getController() {
        return new VideoController(repository, getMapper());
    }

    @Test
    public void shouldReturnVideo() throws Exception {
        Video video = new Video();
        video.setTitle("video title");
        video.setTags("video tags");
        video.setDescription("video description");
        video.setId(1);
        video.setMetaDescription("video meta description");
        video.setSynopsis("video synopsis");
        video.setUrl("video url");
        VideoRating rating = new VideoRating();
        rating.setName("video rating name");
        video.setVideoRating(rating);

        when(repository.get(1)).thenReturn(video);
        mockMvc.perform(get("/videos/{id}", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.links[0].href", is("/videos/1")))
                .andExpect(jsonPath("$.title", is("video title")))
                .andExpect(jsonPath("$.tags", is("video tags")))
                .andExpect(jsonPath("$.description", is("video description")))
                .andExpect(jsonPath("$.metaDescription", is("video meta description")))
                .andExpect(jsonPath("$.synopsis", is("video synopsis")))
                .andExpect(jsonPath("$.url", is("video url")))
                .andExpect(jsonPath("$.rating", is("video rating name")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }
}
