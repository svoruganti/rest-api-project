package com.carnews.rest.api.tests.helper;

import com.carnews.rest.api.configuration.MappingConfiguration;
import com.carnews.rest.api.helper.ClassHelper;
import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.response.ArticleResponse;
import com.carnews.rest.api.response.ContentResponse;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ClassHelperTests {
    @Test
    public void shouldGetContentResponseClass() throws Exception {
        Article article = new Article();
        Class cls = ClassHelper.getContentResponseClass(article.getClass());
        assertThat(cls, Matchers.<Class>is(ArticleResponse.class));
    }

    @Test
    public void shouldGetMappedContentResponse() throws Exception {
        Article article = new Article();
        article.setTags("article tags");
        article.setTitle("article title");
        article.setDescription("article description");

        ContentResponse response = ClassHelper.getMappedContentResponse(getMapper(), article);
        assertThat(response.getTitle(), is("article title"));
        assertThat(response.getTags(), is("article tags"));
        assertThat(response.getDescription(), is("article description"));
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionIfContentHasNoCorrespondingResponseClass() throws Exception {
        TestContent content = new TestContent();
        ClassHelper.getContentResponseClass(content.getClass());
    }

    private Mapper getMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();
        for(BeanMappingBuilder builder : MappingConfiguration.getBeanMappingBuilders())
            mapper.addMapping(builder);
        return mapper;
    }

    private class TestContent extends Content {

    }
}
