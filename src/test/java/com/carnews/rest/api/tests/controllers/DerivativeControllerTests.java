package com.carnews.rest.api.tests.controllers;

import com.carnews.rest.api.controllers.DerivativeController;
import com.carnews.rest.api.models.Derivative;
import com.carnews.rest.api.models.DerivativeExtra;
import com.carnews.rest.api.models.DerivativeExtraType;
import com.carnews.rest.api.models.FuelType;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class DerivativeControllerTests extends AbstractControllerTest {

    @Mock
    private ReadRepository<Derivative> repository;

    @Override
    protected Object getController() {
        return new DerivativeController(repository, getMapper());
    }

    @Test
    public void shouldReturnDerivative() throws Exception {
        Derivative derivative = new Derivative();
        derivative.setId(1);
        derivative.setName("derivative name");
        derivative.setFuelType(FuelType.diesel);
        derivative.setMaxPrice(20000);
        derivative.setMinPrice(10000);
        derivative.setVehicleType("vehicle type");
        derivative.setTrim("trim");
        DerivativeExtraType derivativeExtraType = new DerivativeExtraType();
        derivativeExtraType.setName("extra name");
        DerivativeExtra extra = new DerivativeExtra();
        extra.setType(derivativeExtraType);
        extra.setValue("extra value");
        derivative.setExtras(Arrays.asList(extra));

        when(repository.get(1)).thenReturn(derivative);
        mockMvc.perform(get("/derivatives/{id}", 1).accept(APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is("derivative name")))
                .andExpect(jsonPath("$.fuelType", is("diesel")))
                .andExpect(jsonPath("$.maxPrice", is(20000d)))
                .andExpect(jsonPath("$.minPrice", is(10000d)))
                .andExpect(jsonPath("$.vehicleType", is("vehicle type")))
                .andExpect(jsonPath("$.extras[0].name", is("extra name")))
                .andExpect(jsonPath("$.links[0].href", is("/derivatives/1")))
                .andExpect(jsonPath("$.extras[0].value", is("extra value")));
        verify(repository, times(1)).get(1);
        verifyNoMoreInteractions(repository);
    }
}
