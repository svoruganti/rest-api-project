package com.carnews.rest.api.persistence;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class GalleryImagePK implements Serializable {
    int gallery;
    int image;
}

