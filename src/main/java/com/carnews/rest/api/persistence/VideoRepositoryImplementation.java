package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Video;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.springframework.stereotype.Repository;

@Repository
public class VideoRepositoryImplementation extends AbstractContentReadRepository<Video> implements ContentReadRepository<Video> {
}
