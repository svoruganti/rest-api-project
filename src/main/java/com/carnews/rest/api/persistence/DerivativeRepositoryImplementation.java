package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Derivative;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.springframework.stereotype.Repository;

@Repository
public class DerivativeRepositoryImplementation extends AbstractReadRepository<Derivative> implements ReadRepository<Derivative>{
}
