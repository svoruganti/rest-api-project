package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.persistence.interfaces.ContentRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Repository
public class ContentRepositoryImplementation extends AbstractContentReadRepository<Content> implements ContentRepository {
    @Override
    public Collection<Content> search(String searchTerm, int numberOfItems, int pageIndex) {
        String queryString = "from Content c where (c.title like :term) or (c.tags like :term) or (c.description like :term) or (c.metaDescription like :term)";
        TypedQuery<Content> query = entityManager().createQuery(queryString, Content.class);
        query.setParameter("term", "%" + searchTerm + "%");
        query.setFirstResult(pageIndex * numberOfItems);
        query.setMaxResults(numberOfItems);


        Query countQuery = entityManager().createQuery("select count (*) " + queryString);
        countQuery.setParameter("term", "%" + searchTerm + "%");
        int count = ((Long)countQuery.getSingleResult()).intValue();

        return new Collection<Content> (count, query.getResultList());
    }
}
