package com.carnews.rest.api.persistence;

import com.carnews.rest.api.helper.CacheKeys;
import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Manufacturer;
import com.carnews.rest.api.persistence.interfaces.ManufacturerRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class ManufacturerRepositoryImplementation extends AbstractReadRepository<Manufacturer> implements ManufacturerRepository {
    @Override
    public Collection<Manufacturer> getAll() {
        TypedQuery<Manufacturer> query = entityManager().createQuery("from Manufacturer m", Manufacturer.class);
        List<Manufacturer> manufacturers = query.getResultList();
        return new Collection<Manufacturer>(manufacturers.size(), manufacturers);
    }

    @Override
    @org.springframework.cache.annotation.Cacheable(CacheKeys.CARS)
    public Collection<Car> getAllCarsByManufacturer(int manufacturerId) {
        TypedQuery<Car> query = entityManager().createQuery("from Car c where c.manufacturer.id = :manufacturerId", Car.class);
        query.setParameter("manufacturerId", manufacturerId);
        List<Car> cars = query.getResultList();
        return new Collection<Car>(cars.size(), cars);
    }
}
