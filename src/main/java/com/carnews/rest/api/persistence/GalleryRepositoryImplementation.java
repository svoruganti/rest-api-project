package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Gallery;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.springframework.stereotype.Repository;

@Repository
public class GalleryRepositoryImplementation extends AbstractContentReadRepository<Gallery> implements ContentReadRepository<Gallery> {
}
