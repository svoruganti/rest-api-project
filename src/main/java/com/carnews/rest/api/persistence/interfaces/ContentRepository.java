package com.carnews.rest.api.persistence.interfaces;

import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Content;

public interface ContentRepository extends ContentReadRepository<Content> {
    Collection<Content> search(String searchTerm, int numberOfItems, int skip);
}
