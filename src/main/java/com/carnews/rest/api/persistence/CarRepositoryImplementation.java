package com.carnews.rest.api.persistence;

import com.carnews.rest.api.helper.CacheKeys;
import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Verdict;
import com.carnews.rest.api.persistence.interfaces.CarRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class CarRepositoryImplementation extends AbstractReadRepository<Car> implements CarRepository {
    @Override
    @Cacheable(CacheKeys.VERDICTS)
    public Collection<Verdict> findVerdictsByCar(int carId) {
        TypedQuery<Verdict> query = entityManager().createQuery("from Verdict v where v.derivative.car.id = :carId", Verdict.class);
        query.setParameter("carId", carId);
        List<Verdict> verdicts = query.getResultList();
        return new Collection(verdicts.size(), verdicts);
    }
}
