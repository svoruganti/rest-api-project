package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Image;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ImageRepositoryImplementation extends AbstractReadRepository<Image> implements ReadRepository<Image> {
}
