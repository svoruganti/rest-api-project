package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.PersistableEntity;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class AbstractReadRepository<T extends PersistableEntity> implements ReadRepository<T> {
    @Autowired
    protected EntityManagerFactory entityManagerFactory;

    @SuppressWarnings("unchecked")
    protected Class<T> getClassType() {
        Type t = getClass().getGenericSuperclass();
        return (Class<T>) ((ParameterizedType) t).getActualTypeArguments()[0];
    }

    protected EntityManager entityManager()
    {
        return entityManagerFactory.createEntityManager();
    }

    public T get(Integer id) {
        return entityManager().find(getClassType(), id);
    }
}

