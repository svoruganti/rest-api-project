package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Verdict;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import org.springframework.stereotype.Repository;

@Repository
public class VerdictRepositoryImplementation extends AbstractReadRepository<Verdict> implements ReadRepository<Verdict> {
}
