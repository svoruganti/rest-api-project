package com.carnews.rest.api.persistence.interfaces;

import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Manufacturer;

public interface ManufacturerRepository extends ReadRepository<Manufacturer> {
    Collection<Manufacturer> getAll();
    Collection<Car> getAllCarsByManufacturer(int manufacturerId);
}
