package com.carnews.rest.api.persistence.interfaces;

import com.carnews.rest.api.models.PersistableEntity;

public interface ReadRepository<T extends PersistableEntity> {
    T get(Integer id);
}

