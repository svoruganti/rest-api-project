package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ArticleRepositoryImplementation extends AbstractContentReadRepository<Article> implements ContentReadRepository<Article> {
}
