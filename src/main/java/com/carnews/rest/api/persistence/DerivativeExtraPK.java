package com.carnews.rest.api.persistence;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DerivativeExtraPK implements Serializable {
    int type;
    int derivative;
}
