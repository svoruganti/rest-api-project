package com.carnews.rest.api.persistence;

import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Constants;
import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractContentReadRepository<T extends Content> extends AbstractReadRepository<T> implements ContentReadRepository<T> {

    @Override
    public Collection<T> findLatest(Integer numberOfItems, Integer pageIndex) {
        TypedQuery<T> typedQuery = getTypedQuery();
        typedQuery.setMaxResults(numberOfItems);
        typedQuery.setFirstResult(pageIndex * numberOfItems);
        List<T> list = typedQuery.getResultList();
        return new Collection<T>(list.size(), list);
    }

    protected TypedQuery<T> getTypedQuery() {
        CriteriaBuilder criteriaBuilder = entityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getClassType());
        Root<T> root = criteriaQuery.from(getClassType());
        criteriaQuery.select(root).orderBy(criteriaBuilder.desc(root.get(Constants.PUBLISHED_DATE)));
        return entityManager().createQuery(criteriaQuery);
    }
}

