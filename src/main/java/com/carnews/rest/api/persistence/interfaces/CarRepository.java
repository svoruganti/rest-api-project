package com.carnews.rest.api.persistence.interfaces;

import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Verdict;

public interface CarRepository extends ReadRepository<Car> {
    Collection<Verdict> findVerdictsByCar(int carId);
}
