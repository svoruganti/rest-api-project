package com.carnews.rest.api.persistence.interfaces;

import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Content;

public interface ContentReadRepository<T extends Content> extends ReadRepository<T> {
    Collection<T> findLatest(Integer length, Integer skip);
}
