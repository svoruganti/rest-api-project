package com.carnews.rest.api.exceptions;

public class CheckLengthException extends RuntimeException {
    public CheckLengthException(String message) {
        super(message);
    }
}
