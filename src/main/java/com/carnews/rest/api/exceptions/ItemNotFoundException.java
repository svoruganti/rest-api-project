package com.carnews.rest.api.exceptions;

public class ItemNotFoundException extends RuntimeException {
        public ItemNotFoundException(String message){
        super(message);
    }
}
