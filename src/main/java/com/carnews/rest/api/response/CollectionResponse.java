package com.carnews.rest.api.response;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement(name="items")
@XmlSeeAlso({ArticleResponse.class,
        CarResponse.class,
        GalleryResponse.class,
        ImageResponse.class,
        DerivativeResponse.class,
        ManufacturerResponse.class,
        VerdictResponse.class,
        VideoResponse.class,
        SearchResponse.class,
        LatestResponse.class})
public class CollectionResponse<T>{
    private List<T> items;
    private int count;

    public CollectionResponse() {
    }
    public CollectionResponse(int count, List<T> items) {
        this.items = items;
        this.count = count;
    }

    @XmlAnyElement(lax=true)
    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    @XmlAttribute
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
