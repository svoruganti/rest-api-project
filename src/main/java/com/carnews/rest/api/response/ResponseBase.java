package com.carnews.rest.api.response;

import com.carnews.rest.api.models.PersistableEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlSeeAlso({LinkResponse.class})
public abstract class ResponseBase<T extends PersistableEntity> implements Serializable {
    private int id;

    protected int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected abstract String getHrefValue();
    @XmlElementWrapper(name="links")
    @XmlElement(name="link")
    public List<LinkResponse> getLinks() {
        List<LinkResponse> links = getNavigationLinks();
        LinkResponse self = new LinkResponse("self", getHrefValue());
        links.add(self);
        return links;
    }

    protected List<LinkResponse> getNavigationLinks() {
        return new ArrayList<LinkResponse>();
    }
}
