package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Article;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "article")
public class ArticleResponse extends ContentResponse<Article> {
    @Override
    protected String getHrefValue() {
        return "/articles/" + getId();
    }
}

