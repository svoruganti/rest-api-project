package com.carnews.rest.api.response;

import java.util.ArrayList;
import java.util.List;

public class DefaultResponse {
    public List<LinkResponse> getLinks() {
        List<LinkResponse> responses = new ArrayList<LinkResponse>();
        responses.add(new LinkResponse("latest", "/latest"));
        responses.add(new LinkResponse("articleLatest", "/articles/latest"));
        responses.add(new LinkResponse("galleryLatest", "/galleries/latest"));
        responses.add(new LinkResponse("videoLatest", "/videos/latest"));
        responses.add(new LinkResponse("manufacturers", "/manufacturers"));
        return responses;
    }
}
