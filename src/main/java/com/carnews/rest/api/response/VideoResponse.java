package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Video;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="video")
public class VideoResponse extends ContentResponse<Video> {
    private String url;
    private String rating;

    @Override
    protected String getHrefValue() {
        return "/videos/" + getId();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
