package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Manufacturer;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
import java.util.List;

@XmlRootElement(name = "manufacturer")
public class ManufacturerResponse extends ResponseBase<Manufacturer> {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected String getHrefValue() {
        return "/manufacturers/" + getId();
    }

    @Override
    protected List<LinkResponse> getNavigationLinks() {
        return Arrays.asList(new LinkResponse("cars", String.format("/manufacturers/%s/cars", getId())));
    }
}
