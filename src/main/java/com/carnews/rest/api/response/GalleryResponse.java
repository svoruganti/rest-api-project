package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Gallery;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement(name="gallery")
@XmlSeeAlso(GalleryImageResponse.class)
public class GalleryResponse extends ContentResponse<Gallery> {
    private List<GalleryImageResponse> images;

    @XmlElementWrapper
    @XmlAnyElement(lax=true)
    public List<GalleryImageResponse> getImages() {
        return images;
    }

    public void setImages(List<GalleryImageResponse> images) {
        this.images = images;
    }

    @Override
    protected String getHrefValue() {
        return "/galleries/" + getId();
    }
}
