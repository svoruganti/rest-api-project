package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Image;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="image")
public class ImageResponse extends ResponseBase<Image> {
    private String url;
    private String alternateText;
    private int width;
    private int height;
    private String aspectRatio;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAlternateText() {
        return alternateText;
    }

    public void setAlternateText(String alternateText) {
        this.alternateText = alternateText;
    }

    @Override
    protected String getHrefValue() {
        return "/images/" + getId();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(String aspectRatio) {
        this.aspectRatio = aspectRatio;
    }
}
