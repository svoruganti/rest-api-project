package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Derivative;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "derivative")
public class DerivativeResponse extends ResponseBase<Derivative> {
    private String name;
    private String vehicleType;
    private String trim;
    private String fuelType;
    private double minPrice;
    private double maxPrice;

    private List<DerivativeExtraResponse> extras;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getTrim() {
        return trim;
    }

    public void setTrim(String trim) {
        this.trim = trim;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public List<DerivativeExtraResponse> getExtras() {
        return extras;
    }

    @XmlElementWrapper(name = "extras")
    @XmlElement(name="extra")
    public void setExtras(List<DerivativeExtraResponse> extras) {
        this.extras = extras;
    }

    @Override
    protected String getHrefValue() {
        return "/derivatives/" + getId();
    }
}
