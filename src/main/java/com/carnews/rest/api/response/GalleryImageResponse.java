package com.carnews.rest.api.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "galleryImage")
public class GalleryImageResponse {
    private ImageResponse image;
    private int sortOrder;

    public ImageResponse getImage() {
        return image;
    }

    public void setImage(ImageResponse image) {
        this.image = image;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }
}
