package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Car;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "car")
public class CarResponse extends ResponseBase<Car>{
    private String make;
    private String model;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    protected String getHrefValue() {
        return "/cars/" + getId();
    }

    @Override
    protected List<LinkResponse> getNavigationLinks() {
        List<LinkResponse> links = new ArrayList<LinkResponse>();
        LinkResponse verdicts = new LinkResponse("verdicts", String.format("/cars/%s/verdicts", getId()));
        links.add(verdicts);
        LinkResponse derivatives = new LinkResponse("derivatives", String.format("/cars/%s/derivatives", getId()));
        links.add(derivatives);
        LinkResponse videos = new LinkResponse("videos", String.format("/cars/%s/videos", getId()));
        links.add(videos);
        LinkResponse rivals = new LinkResponse("rivals", String.format("/cars/%s/rivals", getId()));
        links.add(rivals);

        return links;
    }
}
