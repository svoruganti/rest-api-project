package com.carnews.rest.api.response;

import com.carnews.rest.api.models.Content;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public abstract class ContentResponse<T extends Content> extends ResponseBase<T> {
    private Date publishedDate;
    private String tags;
    private String title;
    private String description;
    private String synopsis;
    private String metaDescription;

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }
}
