package com.carnews.rest.api.controllers;

import com.carnews.rest.api.exceptions.CheckLengthException;
import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import com.carnews.rest.api.response.CollectionResponse;
import com.carnews.rest.api.response.ContentResponse;
import org.dozer.Mapper;
import org.springframework.web.bind.annotation.*;

@RestController
public abstract class AbstractContentController<T extends Content, Y extends ContentResponse> extends AbstractController<T, Y>{
    private ContentReadRepository<T> contentReadRepository;
    protected AbstractContentController(ContentReadRepository<T> contentReadRepository, Mapper mapper){
        super(contentReadRepository, mapper);
        this.contentReadRepository = contentReadRepository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/latest")
    public CollectionResponse<Y> getLatest(@RequestParam(value = "skip", required = false, defaultValue = "0") int skip, @RequestParam(value = "size", required = false, defaultValue = "100") int size){
        if (size > 100)
            throw new CheckLengthException("Maximum number of latest results allowed is 100");
        return getCollectionResponse(contentReadRepository.findLatest(size, skip));
    }
}
