package com.carnews.rest.api.controllers;

import com.carnews.rest.api.models.Derivative;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import com.carnews.rest.api.response.DerivativeResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/derivatives")
public class DerivativeController extends AbstractController<Derivative, DerivativeResponse> {
    @Autowired
    public DerivativeController(ReadRepository<Derivative> readRepository, Mapper mapper) {
        super(readRepository, mapper);
    }
}
