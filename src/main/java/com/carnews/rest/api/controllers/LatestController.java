package com.carnews.rest.api.controllers;

import com.carnews.rest.api.exceptions.CheckLengthException;
import com.carnews.rest.api.helper.ClassHelper;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.persistence.interfaces.ContentRepository;
import com.carnews.rest.api.response.CollectionResponse;
import com.carnews.rest.api.response.LatestResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/latest")
@RestController
public class LatestController{
    private final ContentRepository contentReadRepository;
    private final Mapper mapper;

    @Autowired
    public LatestController(ContentRepository contentReadRepository, Mapper mapper) {
        this.contentReadRepository = contentReadRepository;
        this.mapper = mapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    public CollectionResponse<LatestResponse> getLatest(@RequestParam(value = "skip", required = false, defaultValue = "0") int skip, @RequestParam(value = "size", required = false, defaultValue = "100") int size) throws Exception {
        if (size > 100)
            throw new CheckLengthException("Maximum allowed size is 100");
        Collection<Content> contents = contentReadRepository.findLatest(size, skip);
        return new CollectionResponse<LatestResponse>(contents.getCount(), getLatestResponses(contents));
    }

    private List<LatestResponse> getLatestResponses(Collection<Content> contents) throws Exception {
        List<LatestResponse> responses = new ArrayList<LatestResponse>();
        for (Content content : contents.getItems()) {
            LatestResponse latestResponse = mapper.map(ClassHelper.getMappedContentResponse(mapper, content), LatestResponse.class);
            latestResponse.setContentType(content.getClass().getSimpleName());
            responses.add(latestResponse);
        }
        return responses;
    }
}
