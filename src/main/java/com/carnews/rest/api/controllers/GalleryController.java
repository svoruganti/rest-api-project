package com.carnews.rest.api.controllers;

import com.carnews.rest.api.models.Gallery;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import com.carnews.rest.api.response.GalleryResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/galleries")
public class GalleryController extends AbstractContentController<Gallery, GalleryResponse> {
    @Autowired
    public GalleryController(ContentReadRepository<Gallery> contentReadRepository, Mapper mapper) {
        super(contentReadRepository, mapper);
    }
}
