package com.carnews.rest.api.controllers;

import com.carnews.rest.api.exceptions.CollectionEmptyException;
import com.carnews.rest.api.exceptions.ItemNotFoundException;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.PersistableEntity;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import com.carnews.rest.api.response.CollectionResponse;
import com.carnews.rest.api.response.ResponseBase;
import org.dozer.Mapper;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@RestController
public abstract class AbstractController<T extends PersistableEntity, Y extends ResponseBase> {
    private ReadRepository<T> readRepository;
    protected Mapper mapper;

    protected AbstractController(ReadRepository<T> readRepository, Mapper mapper) {
        this.readRepository = readRepository;
        this.mapper = mapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @ResponseBody
    public Y get(@PathVariable int id) {
        T t = readRepository.get(id);
        if (t == null)
            throw new ItemNotFoundException(String.format("Object type of '%s' with id '%d' not found", getDomainClassType(), id));
        return mapper.map(t, getResponseClassType());
    }

    @SuppressWarnings("unchecked")
    protected Class<T> getDomainClassType() {
        Type t = getClass().getGenericSuperclass();
        return (Class<T>) ((ParameterizedType) t).getActualTypeArguments()[0];
    }

    @SuppressWarnings("unchecked")
    protected Class<Y> getResponseClassType() {
        Type y = getClass().getGenericSuperclass();
        return (Class<Y>) ((ParameterizedType) y).getActualTypeArguments()[1];
    }

    protected CollectionResponse<Y> getCollectionResponse(Collection<T> collection) {
        return getCollectionResponse(collection, getResponseClassType());
    }

    protected <T extends PersistableEntity, Y extends ResponseBase> CollectionResponse<Y>  getCollectionResponse(Collection<T> collection, Class<Y> destination) {
        List<Y> responses = new ArrayList<Y>();
        if (collection.getCount() == 0)
            throw new CollectionEmptyException();
        for(T t : collection.getItems())
            responses.add(mapper.map(t, destination));
        return new CollectionResponse<Y>(collection.getCount(), responses);
    }

    protected <T extends PersistableEntity> Collection<T> getCollection(List<T> items) {
        return new Collection(items.size(), items);
    }
}
