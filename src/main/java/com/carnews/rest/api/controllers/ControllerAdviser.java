package com.carnews.rest.api.controllers;

import com.carnews.rest.api.exceptions.CheckLengthException;
import com.carnews.rest.api.exceptions.CollectionEmptyException;
import com.carnews.rest.api.exceptions.ItemNotFoundException;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerAdviser {

    static final Logger logger = Logger.getLogger(ControllerAdviser.class);

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Specified resource doesn't exist")
    @ExceptionHandler(value = ItemNotFoundException.class)
    public void handleItemNotFoundException(ItemNotFoundException infe){
        logger.info(infe.getMessage());
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @ExceptionHandler(value = CollectionEmptyException.class)
    public void handleCollectionEmptyException() {
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public void handleGenericException(Exception ex) {
        logger.error(String.format("Object '%s' has thrown '%s' error", ex.getStackTrace(), ex.getMessage()));
    }

    @ExceptionHandler(value = CheckLengthException.class)
    public ResponseEntity<String> handleCheckLengthException(CheckLengthException exception){
        return new ResponseEntity<String>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
