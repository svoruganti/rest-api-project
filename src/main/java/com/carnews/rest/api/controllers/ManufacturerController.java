package com.carnews.rest.api.controllers;

import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Manufacturer;
import com.carnews.rest.api.persistence.interfaces.ManufacturerRepository;
import com.carnews.rest.api.response.CarResponse;
import com.carnews.rest.api.response.CollectionResponse;
import com.carnews.rest.api.response.ManufacturerResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manufacturers")
public class ManufacturerController extends AbstractController<Manufacturer, ManufacturerResponse> {
    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerController(ManufacturerRepository manufacturerRepository, Mapper mapper) {
        super(manufacturerRepository, mapper);
        this.manufacturerRepository = manufacturerRepository;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public CollectionResponse<ManufacturerResponse> getAllManufacturers() {
        Collection<Manufacturer> manufacturers = manufacturerRepository.getAll();
        return getCollectionResponse(manufacturers);
    }

    @RequestMapping(value = "/{id}/cars", method = RequestMethod.GET)
    public CollectionResponse<CarResponse> getAllCarsByManufacturer(@PathVariable int id) {
        Collection<Car> cars = manufacturerRepository.getAllCarsByManufacturer(id);
        return getCollectionResponse(cars, CarResponse.class);
    }
}
