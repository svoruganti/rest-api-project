package com.carnews.rest.api.controllers;

import com.carnews.rest.api.exceptions.CheckLengthException;
import com.carnews.rest.api.helper.ClassHelper;
import com.carnews.rest.api.helper.StringHelper;
import com.carnews.rest.api.models.*;
import com.carnews.rest.api.persistence.interfaces.ContentRepository;
import com.carnews.rest.api.response.*;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/search")
public class SearchController {
    private final ContentRepository contentRepository;
    private final Mapper mapper;

    @Autowired
    public SearchController(ContentRepository contentRepository, Mapper mapper) {
        this.contentRepository = contentRepository;
        this.mapper = mapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    public CollectionResponse<SearchResponse> getSearchResults(@RequestParam(value = "q", required = false, defaultValue = "") String searchTerm, @RequestParam(value = "size", required = false, defaultValue = "100") int numberOfItems, @RequestParam(value = "skip", required = false, defaultValue = "0") int pageIndex) throws Exception {
        if (numberOfItems > 100)
            throw new CheckLengthException("Maximum number of latest results allowed are 100");
        searchTerm = StringHelper.sanitiseString(searchTerm);
        if (searchTerm.length() < 4 || searchTerm.length() > 16)
            throw new CheckLengthException("Search term should be of length between 4 and 16 characters");
        Collection<Content> results = contentRepository.search(searchTerm, numberOfItems, pageIndex);
        return getSearchResponses(results);
    }

    private CollectionResponse<SearchResponse> getSearchResponses(Collection<Content> collection) throws Exception {
        List<SearchResponse> searchResponses = new ArrayList<SearchResponse>();
        for (Content content : collection.getItems())
            searchResponses.add(mapper.map(ClassHelper.getMappedContentResponse(mapper, content), SearchResponse.class));

        return new CollectionResponse<SearchResponse>(collection.getCount(), searchResponses);
    }
}
