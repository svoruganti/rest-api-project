package com.carnews.rest.api.controllers;

import com.carnews.rest.api.models.Car;
import com.carnews.rest.api.models.Collection;
import com.carnews.rest.api.models.Verdict;
import com.carnews.rest.api.persistence.interfaces.CarRepository;
import com.carnews.rest.api.response.*;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cars")
public class CarController extends AbstractController<Car, CarResponse> {
    private final CarRepository carReadRepository;

    @Autowired
    public CarController(CarRepository carReadRepository, Mapper mapper) {
        super(carReadRepository, mapper);
        this.carReadRepository = carReadRepository;
    }

    @RequestMapping(value="/{id}/derivatives", method = RequestMethod.GET)
    public CollectionResponse<DerivativeResponse> getDerivatives(@PathVariable int id) {
        Car car = carReadRepository.get(id);
        return getCollectionResponse(getCollection(car.getDerivatives()), DerivativeResponse.class);
    }

    @RequestMapping(value="/{carId}/verdicts", method = RequestMethod.GET)
    public CollectionResponse<VerdictResponse> getVerdicts(@PathVariable int carId) {
        Collection<Verdict> verdicts = carReadRepository.findVerdictsByCar(carId);
        return getCollectionResponse(verdicts, VerdictResponse.class);
    }

    @RequestMapping(value="/{carId}/videos", method = RequestMethod.GET)
    public CollectionResponse<VideoResponse> getVideos(@PathVariable int carId) {
        Car car = carReadRepository.get(carId);
        return getCollectionResponse(getCollection(car.getVideos()), VideoResponse.class);
    }

    @RequestMapping(value="/{carId}/rivals", method = RequestMethod.GET)
    public CollectionResponse<CarResponse> getRivals(@PathVariable int carId) {
        Car car = carReadRepository.get(carId);
        return getCollectionResponse(getCollection(car.getRivals()));
    }

}
