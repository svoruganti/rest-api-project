package com.carnews.rest.api.controllers;

import org.hibernate.Session;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManagerFactory;

@RestController
@RequestMapping("/caches")
public class CacheController {
    @Autowired
    private EntityManagerFactory entityManager;
    static final Logger logger = Logger.getLogger(CacheController.class);

    public CacheController() {
    }

    @RequestMapping("/clear")
    public void clearCache() {

    }

    @RequestMapping("/print")
    public void printData() {
        Session session = (Session) entityManager.createEntityManager().getDelegate();

        logger.info(session.getSessionFactory().getStatistics().toString());
    }
}
