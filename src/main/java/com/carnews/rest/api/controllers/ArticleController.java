package com.carnews.rest.api.controllers;

import com.carnews.rest.api.models.Article;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import com.carnews.rest.api.response.ArticleResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/articles")
public class ArticleController extends AbstractContentController<Article, ArticleResponse> {

    @Autowired
    public ArticleController(ContentReadRepository<Article> articleRepository, Mapper mapper) {
        super(articleRepository, mapper);
    }
}
