package com.carnews.rest.api.controllers;

import com.carnews.rest.api.models.Video;
import com.carnews.rest.api.persistence.interfaces.ContentReadRepository;
import com.carnews.rest.api.response.VideoResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/videos")
public class VideoController extends AbstractContentController<Video, VideoResponse> {
    @Autowired
    public VideoController(ContentReadRepository<Video> contentReadRepository, Mapper mapper) {
        super(contentReadRepository, mapper);
    }
}
