package com.carnews.rest.api.controllers;

import com.carnews.rest.api.models.Verdict;
import com.carnews.rest.api.persistence.interfaces.ReadRepository;
import com.carnews.rest.api.response.VerdictResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/verdicts")
public class VerdictController extends AbstractController<Verdict, VerdictResponse> {
    @Autowired
    public VerdictController(ReadRepository<Verdict> readRepository, Mapper mapper) {
        super(readRepository, mapper);
    }
}
