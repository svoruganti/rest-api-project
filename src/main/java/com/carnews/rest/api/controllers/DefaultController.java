package com.carnews.rest.api.controllers;

import com.carnews.rest.api.response.DefaultResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class DefaultController {
    @RequestMapping(value = "")
    public DefaultResponse getDefaultResponse() {
        return new DefaultResponse();
    }
}
