package com.carnews.rest.api.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="derivative")
@Cacheable
public class Derivative extends PersistableEntity{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carid", nullable = false)
    private Car car;
    private String vehicleType;
    private String name;
    private String trim;
    @Enumerated(EnumType.STRING)
    private FuelType fuelType;
    private double minPrice;
    private double maxPrice;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "derivative")
    private List<DerivativeExtra> extras;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrim() {
        return trim;
    }

    public void setTrim(String trim) {
        this.trim = trim;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public List<DerivativeExtra> getExtras() {
        return extras;
    }

    public void setExtras(List<DerivativeExtra> extras) {
        this.extras = extras;
    }
}
