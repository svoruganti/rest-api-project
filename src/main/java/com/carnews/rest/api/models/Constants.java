package com.carnews.rest.api.models;

public class Constants {
    public static final String CONTENT_TYPE_ARTICLE = "Article";
    public static final String CONTENT_TYPE_REVIEW = "Review";
    public static final String CONTENT_TYPE_GALLERY = "Gallery";
    public static final String CONTENT_TYPE_VIDEO = "Video";
    public static final String CONTENT_TYPE_IMAGE = "Image";
    public static final String PUBLISHED_DATE = "publishedDate";
}
