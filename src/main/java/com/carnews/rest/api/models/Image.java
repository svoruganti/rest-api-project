package com.carnews.rest.api.models;

import javax.persistence.*;

@Entity
@Table(name = "image")
@Cacheable
public class Image extends PersistableEntity{
    private String alternateText;
    private String url;
    private int width;
    private int height;
    @Enumerated(EnumType.STRING)
    private AspectRatio aspectRatio;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAlternateText() {
        return alternateText;
    }

    public void setAlternateText(String alternateText) {
        this.alternateText = alternateText;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public AspectRatio getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(AspectRatio aspectRatio) {
        this.aspectRatio = aspectRatio;
    }
}