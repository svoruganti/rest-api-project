package com.carnews.rest.api.models;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class PersistableEntity implements Serializable {
    @Column(name = "Id")
    @Id
    private int id;

    public int getId() {
        return id;
    }
    public void setId(int id) { this.id = id; }
}
