package com.carnews.rest.api.models;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="derivativeextratype")
@Cacheable
public class DerivativeExtraType extends PersistableEntity{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
