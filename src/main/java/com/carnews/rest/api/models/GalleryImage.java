package com.carnews.rest.api.models;

import com.carnews.rest.api.persistence.GalleryImagePK;

import javax.persistence.*;

@Entity
@Table(name = "galleryimage")
@IdClass(GalleryImagePK.class)
@Cacheable
public class GalleryImage {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "galleryid")
    private Gallery gallery;

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "imageid")
    private Image image;
    private int sortOrder;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }
}
