package com.carnews.rest.api.models;

import javax.persistence.*;

@Entity
@Table(name = "article")
@DiscriminatorValue(value = Constants.CONTENT_TYPE_ARTICLE)
@Cacheable
public class Article extends Content{
    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "imageid", nullable = true)
    private Image image;
}
