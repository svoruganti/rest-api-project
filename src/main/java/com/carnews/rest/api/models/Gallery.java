package com.carnews.rest.api.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "gallery")
@DiscriminatorValue(value = Constants.CONTENT_TYPE_GALLERY)
@Cacheable
public class Gallery extends Content{
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "gallery")
    private List<GalleryImage> images;

    public List<GalleryImage> getImages() {
        return images;
    }

    public void setImages(List<GalleryImage> images) {
        this.images = images;
    }
}
