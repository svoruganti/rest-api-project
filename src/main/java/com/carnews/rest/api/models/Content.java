package com.carnews.rest.api.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "content")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "content_type", discriminatorType = DiscriminatorType.STRING)
@Cacheable
public class Content extends PersistableEntity {

    @Column(name="publisheddate")
    private Date publishedDate;
    @Column(name="tags")
    private String tags;
    @Column(name="title")
    private String title;
    @Column(name="description")
    private String description;
    @Column(name="synopsis")
    private String synopsis;
    @Column(name="metadescription")
    private String metaDescription;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
