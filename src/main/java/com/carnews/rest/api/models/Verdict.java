package com.carnews.rest.api.models;

import javax.persistence.*;

@Entity
@Table(name="verdict")
@Cacheable
public class Verdict extends PersistableEntity {
    private double verdictRating;
    private String verdictText;
    private double comfortRating;
    private String comfortText;
    private double coolRating;
    private String coolText;
    private double handlingRating;
    private String handlingText;
    private double performanceRating;
    private String performanceText;
    private double practicalityRating;
    private String practicalityText;
    private double qualityRating;
    private String qualityText;
    private double runningCostRating;
    private String runningCostText;
    @OneToOne
    @JoinColumn(name = "derivativeid", nullable = false)
    private Derivative derivative;
    private Boolean isActive;

    public double getVerdictRating() {
        return verdictRating;
    }

    public void setVerdictRating(double verdictRating) {
        this.verdictRating = verdictRating;
    }

    public String getVerdictText() {
        return verdictText;
    }

    public void setVerdictText(String verdictText) {
        this.verdictText = verdictText;
    }

    public double getComfortRating() {
        return comfortRating;
    }

    public void setComfortRating(double comfortRating) {
        this.comfortRating = comfortRating;
    }

    public String getComfortText() {
        return comfortText;
    }

    public void setComfortText(String comfortText) {
        this.comfortText = comfortText;
    }

    public double getCoolRating() {
        return coolRating;
    }

    public void setCoolRating(double coolRating) {
        this.coolRating = coolRating;
    }

    public String getCoolText() {
        return coolText;
    }

    public void setCoolText(String coolText) {
        this.coolText = coolText;
    }

    public double getHandlingRating() {
        return handlingRating;
    }

    public void setHandlingRating(double handlingRating) {
        this.handlingRating = handlingRating;
    }

    public String getHandlingText() {
        return handlingText;
    }

    public void setHandlingText(String handlingText) {
        this.handlingText = handlingText;
    }

    public double getPerformanceRating() {
        return performanceRating;
    }

    public void setPerformanceRating(double performanceRating) {
        this.performanceRating = performanceRating;
    }

    public String getPerformanceText() {
        return performanceText;
    }

    public void setPerformanceText(String performanceText) {
        this.performanceText = performanceText;
    }

    public double getPracticalityRating() {
        return practicalityRating;
    }

    public void setPracticalityRating(double practicalityRating) {
        this.practicalityRating = practicalityRating;
    }

    public String getPracticalityText() {
        return practicalityText;
    }

    public void setPracticalityText(String practicalityText) {
        this.practicalityText = practicalityText;
    }

    public double getQualityRating() {
        return qualityRating;
    }

    public void setQualityRating(double qualityRating) {
        this.qualityRating = qualityRating;
    }

    public String getQualityText() {
        return qualityText;
    }

    public void setQualityText(String qualityText) {
        this.qualityText = qualityText;
    }

    public double getRunningCostRating() {
        return runningCostRating;
    }

    public void setRunningCostRating(double runningCostRating) {
        this.runningCostRating = runningCostRating;
    }

    public String getRunningCostText() {
        return runningCostText;
    }

    public void setRunningCostText(String runningCostText) {
        this.runningCostText = runningCostText;
    }

    public Derivative getDerivative() {
        return derivative;
    }

    public void setDerivative(Derivative derivative) {
        this.derivative = derivative;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
