package com.carnews.rest.api.models;

import java.util.List;

public class Collection<T extends PersistableEntity> {
    private int count;
    private List<T> items;

    public Collection(int count, List<T> items) {
        this.count = count;
        this.items = items;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
