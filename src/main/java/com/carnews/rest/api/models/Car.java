package com.carnews.rest.api.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "car")
@Cacheable
public class Car extends PersistableEntity {
    private String model;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "car")
    private List<Derivative> derivatives;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="manufacturerid", nullable = false)
    private Manufacturer manufacturer;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name="carvideo", joinColumns = @JoinColumn(name = "carid"), inverseJoinColumns = @JoinColumn(name = "videoid"))
    private List<Video> videos;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name="rivalcar", joinColumns = @JoinColumn(name = "carid"), inverseJoinColumns = @JoinColumn(name = "rivalcarid"))
    private List<Car> rivals;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<Derivative> getDerivatives() {
        return derivatives;
    }

    public void setDerivatives(List<Derivative> derivatives) {
        this.derivatives = derivatives;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public List<Car> getRivals() {
        return rivals;
    }

    public void setRivals(List<Car> rivals) {
        this.rivals = rivals;
    }
}
