package com.carnews.rest.api.models;

import com.carnews.rest.api.persistence.DerivativeExtraPK;

import javax.persistence.*;

@Entity
@Table(name="derivativeextra")
@IdClass(DerivativeExtraPK.class)
@Cacheable
public class DerivativeExtra {

    @Id
    @OneToOne
    @JoinColumn(name = "derivativeextratypeid", nullable = false)
    private DerivativeExtraType type;
    private String value;

    @Id
    @ManyToOne
    @JoinColumn(name = "derivativeid", nullable = false)
    private Derivative derivative;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DerivativeExtraType getType() {
        return type;
    }

    public void setType(DerivativeExtraType type) {
        this.type = type;
    }
}
