package com.carnews.rest.api.models;

import javax.persistence.*;

@Entity
@Table(name="video")
@DiscriminatorValue(value = Constants.CONTENT_TYPE_VIDEO)
@Cacheable
public class Video extends Content {
    private String url;
    @OneToOne
    @JoinColumn(name="videoratingid", nullable = false)
    private VideoRating videoRating;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public VideoRating getVideoRating() {
        return videoRating;
    }

    public void setVideoRating(VideoRating videoRating) {
        this.videoRating = videoRating;
    }
}
