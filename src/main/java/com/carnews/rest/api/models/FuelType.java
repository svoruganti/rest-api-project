package com.carnews.rest.api.models;

public enum FuelType {
    petrol,
    diesel
}
