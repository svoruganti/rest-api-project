package com.carnews.rest.api.models;

public enum AspectRatio {
    FourByThree,
    SixteenByNine,
    ThreeByTwo
}
