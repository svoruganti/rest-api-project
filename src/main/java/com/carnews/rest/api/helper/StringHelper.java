package com.carnews.rest.api.helper;

public class StringHelper {
    private StringHelper(){
    }
    public static String sanitiseString(String s) {
        s = s.replaceAll("[\\W]|_", " ");
        return s.trim().replaceAll(" +", " ");
    }
}
