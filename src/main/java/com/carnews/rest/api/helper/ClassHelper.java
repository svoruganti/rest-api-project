package com.carnews.rest.api.helper;

import com.carnews.rest.api.models.Content;
import com.carnews.rest.api.response.ContentResponse;
import org.dozer.Mapper;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.lang.reflect.ParameterizedType;
import java.util.Set;

public class ClassHelper {
    private ClassHelper() {
    }
    public static Class<? extends ContentResponse> getContentResponseClass(Class<? extends Content> clazz) throws Exception {
        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new AssignableTypeFilter(ContentResponse.class));
        Set<BeanDefinition> components = provider.findCandidateComponents(ContentResponse.class.getPackage().getName());
        for(BeanDefinition beanDefinition : components) {
            Class cls = Class.forName(beanDefinition.getBeanClassName());
            if (((ParameterizedType) cls.getGenericSuperclass()).getActualTypeArguments()[0] == clazz)
                return cls;
        }

        throw new Exception(String.format("Response class for class type '%s' doesn't exit", clazz.toString()));
    }

    public static ContentResponse getMappedContentResponse(Mapper mapper, Content content) throws Exception {
        return mapper.map(content, getContentResponseClass(content.getClass()));
    }
}
