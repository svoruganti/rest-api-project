package com.carnews.rest.api.helper;

public class CacheKeys {
    public static final String CARS = "com.carnews.rest.api.models.Car";
    public static final String VERDICTS = "com.carnews.rest.api.models.Verdict";
    public static final String ARTICLES = "com.carnews.rest.api.models.Article";
    public static final String DERIVATIVES = "com.carnews.rest.api.models.Derivative";
}
