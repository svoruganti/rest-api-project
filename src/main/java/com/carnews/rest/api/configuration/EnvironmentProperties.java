package com.carnews.rest.api.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class EnvironmentProperties {
    private static volatile EnvironmentProperties environmentProperties = null;
    private EnvironmentProperties() {
    }

    public static EnvironmentProperties getInstance(){
        if (environmentProperties == null) {
            synchronized (EnvironmentProperties.class){
                if (environmentProperties == null){
                    environmentProperties = new EnvironmentProperties();
                }
            }
        }
        return environmentProperties;
    }

    public Properties getProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("/application.properties"));
        return properties;
    }

    public String getProperty(String key) throws IOException {
        return getProperties().getProperty(key);
    }
}
