package com.carnews.rest.api.configuration;

import com.carnews.rest.api.models.PersistableEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.Resource;
import javax.persistence.SharedCacheMode;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(basePackages = {"com.carnews.rest.api.persistence"})
@ComponentScan({"com.carnews.rest.api.persistence"})
@PropertySource("classpath:application.properties")
public class HibernateConfiguration
{
    @Resource
    private Environment environment;

    @Bean
    public DataSource dataSource()
    {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(environment.getProperty("database.driver.class.name"));
        driverManagerDataSource.setUrl(environment.getProperty("database.url"));
        driverManagerDataSource.setUsername(environment.getProperty("database.user"));
        driverManagerDataSource.setPassword(environment.getProperty("database.password"));
        return driverManagerDataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setSharedCacheMode(SharedCacheMode.ENABLE_SELECTIVE);
        //entityManagerFactoryBean.getJpaPropertyMap().put("hibernate.cache.provider_class", "net.sf.ehcache.hibernate.EhCacheProvider");
        entityManagerFactoryBean.getJpaPropertyMap().put("hibernate.cache.use_second_level_cache", "true");
        entityManagerFactoryBean.getJpaPropertyMap().put("hibernate.cache.use_query_cache", "true");
        entityManagerFactoryBean.getJpaPropertyMap().put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
        entityManagerFactoryBean.getJpaPropertyMap().put("hibernate.cache.default_cache_concurrency_strategy", "read-only");
        entityManagerFactoryBean.getJpaPropertyMap().put("hibernate.generate_statistics", "true");
        entityManagerFactoryBean.setPackagesToScan(PersistableEntity.class.getPackage().getName());

        return entityManagerFactoryBean;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(false);
        hibernateJpaVendorAdapter.setDatabase(Database.POSTGRESQL);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager();
    }
}
