package com.carnews.rest.api.configuration;

import com.carnews.rest.api.models.*;
import com.carnews.rest.api.response.*;
import org.dozer.loader.api.BeanMappingBuilder;

import java.util.ArrayList;
import java.util.List;

public class MappingConfiguration {
    public static BeanMappingBuilder getCarMapper() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(Car.class, CarResponse.class)
                        .fields("manufacturer.name", "make");
            }
        };
    }

    public static BeanMappingBuilder getDerivativeExtraMapper() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(DerivativeExtra.class, DerivativeExtraResponse.class)
                        .fields("type.name", "name");
            }
        };
    }

    public static BeanMappingBuilder getVerdictMapper() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(Verdict.class, VerdictResponse.class)
                        .fields("derivative.name", "derivativeName")
                        .fields("derivative.car.model", "model")
                        .fields("derivative.car.manufacturer.name", "make");
            }
        };
    }

    public static BeanMappingBuilder getVideoMapper() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(Video.class, VideoResponse.class)
                        .fields("videoRating.name", "rating");
            }
        };
    }

    public static List<BeanMappingBuilder> getBeanMappingBuilders() {
        List<BeanMappingBuilder> builders = new ArrayList<BeanMappingBuilder>();
        builders.add(getCarMapper());
        builders.add(getDerivativeExtraMapper());
        builders.add(getVerdictMapper());
        builders.add(getVideoMapper());
        return builders;
    }
}
