CREATE FUNCTION article_content_type() RETURNS TRIGGER AS $article_content_type$
  BEGIN
    IF NOT EXISTS (SELECT * FROM Content WHERE id = NEW.Id AND content_type='Article') THEN
      RAISE EXCEPTION 'Check the content type of the article id %', NEW.id;
    END IF;
    RETURN NEW;
  END;
$article_content_type$ LANGUAGE plpgsql;

CREATE TRIGGER article_content_type BEFORE INSERT ON article
  FOR EACH ROW EXECUTE PROCEDURE article_content_type();