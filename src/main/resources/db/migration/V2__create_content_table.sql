CREATE TABLE content
(
  publisheddate date NOT NULL,
  tags character varying,
  description text NOT NULL,
  synopsis character varying,
  content_type character varying NOT NULL,
  id serial NOT NULL primary key,
  metadescription character varying,
  title character varying NOT NULL
)