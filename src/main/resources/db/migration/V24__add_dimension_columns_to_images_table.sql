ALTER TABLE image
    ADD COLUMN width INT DEFAULT (0),
    ADD COLUMN height INT DEFAULT (0),
    ADD COLUMN aspectRatio varchar(24) null
