CREATE TABLE DerivativeExtra(
  Value VARCHAR(256),
  DerivativeId INT NOT NULL REFERENCES Derivative(Id),
  DerivativeExtraTypeId INT NOT NULL REFERENCES DerivativeExtraType(Id),
  PRIMARY KEY (DerivativeId, DerivativeExtraTypeId)
)