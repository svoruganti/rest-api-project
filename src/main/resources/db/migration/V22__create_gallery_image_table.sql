CREATE TABLE GalleryImage (
  GalleryId INT NOT NULL REFERENCES gallery(id),
  ImageId INT NOT NULL REFERENCES image(id),
  PRIMARY KEY (GalleryId, ImageId)
)