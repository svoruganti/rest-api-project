CREATE TABLE Derivative(
  Id SERIAL PRIMARY KEY,
  VehicleType VARCHAR(128),
  Name VARCHAR (128) NOT NULL,
  Trim VARCHAR (128),
  FuelType VARCHAR (24),
  MinPrice DOUBLE PRECISION DEFAULT (0),
  MaxPrice DOUBLE PRECISION DEFAULT (0) CHECK (MaxPrice >= MinPrice),
  CarId INT NOT NULL REFERENCES Car(Id)
)