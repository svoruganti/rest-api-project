ALTER TABLE content
    ADD CHECK (content_type = 'Article' OR content_type = 'Video' OR content_type = 'Gallery')