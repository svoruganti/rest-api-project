CREATE FUNCTION gallery_content_type() RETURNS TRIGGER AS $gallery_content_type$
  BEGIN
    IF NOT EXISTS (SELECT * FROM Content WHERE id = NEW.Id AND content_type='Gallery') THEN
      RAISE EXCEPTION 'Check the content type of the gallery id %', NEW.id;
    END IF;
    RETURN NEW;
  END;
$gallery_content_type$ LANGUAGE plpgsql;

CREATE TRIGGER gallery_content_type BEFORE INSERT ON gallery
FOR EACH ROW EXECUTE PROCEDURE gallery_content_type();